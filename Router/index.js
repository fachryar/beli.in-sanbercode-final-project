import React from 'react';
import { RefreshControlBase, StyleSheet, Text, View, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';

import ProfileScreen from '../Pages/ProfileScreen';
import LoginScreen from '../Pages/LoginScreen';
import RegisterScreen from '../Pages/RegisterScreen';
import CartScreen from '../Pages/CartScreen';
import HomeScreen from '../Pages/HomeScreen';
import DetailScreen from '../Pages/DetailScreen';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();



export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="LoginScreen">
                <Stack.Screen options={{headerShown: false}} name="LoginScreen" component={LoginScreen}/>
                <Stack.Screen options={{headerShown: false}} name="RegisterScreen" component={RegisterScreen}/>
                <Stack.Screen options={{headerShown: false}} name="MainApp" component={MainApp}/>
                <Stack.Screen options={{headerShown: false}} name="DetailScreen" component={DetailScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp =()=>(
    <Tab.Navigator 
        tabBarOptions={{
            showLabel:false, 
            style: {
                position:'absolute',
                height: 64,
                borderTopLeftRadius: 16,
                borderTopRightRadius: 16,
            }
        }}>
        <Tab.Screen name="Home" component={HomeScreen} options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center',}}>
                <Image
                    source={require('../assets/icon/home.png')}
                    resizeMode='contain'
                    style={{
                        width: 20,
                        height: 20,
                        marginTop:4,
                        tintColor: focused ? '#F96C6C' : '#909394'
                    }}
                />
                <Text style={{color: focused ? '#F96C6C' : '#909394', fontSize: 12}}>Home</Text>
            </View>
        )}}     
        />
        <Tab.Screen name="Cart" component={CartScreen} options={{
        tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center',}}>
                <Image
                    source={require('../assets/icon/cart.png')}
                    resizeMode='contain'
                    style={{
                        width: 20,
                        height: 20,
                        marginTop:4,
                        tintColor: focused ? '#F96C6C' : '#909394'
                    }}
                />
                <Text style={{color: focused ? '#F96C6C' : '#909394', fontSize: 12}}>Cart</Text>
            </View>
        )}}
        />
        <Tab.Screen name="Profile" component={ProfileScreen} options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({focused}) => (
            <View style={{alignItems: 'center', justifyContent: 'center',}}>
                <Image
                    source={require('../assets/icon/user.png')}
                    resizeMode='contain'
                    style={{
                        width: 20,
                        height: 20,
                        marginTop:4,
                        tintColor: focused ? '#F96C6C' : '#909394'
                    }}
                />
                <Text style={{color: focused ? '#F96C6C' : '#909394', fontSize: 12}}>Profile</Text>
            </View>
        )}}
        /> 
    </Tab.Navigator>
)
