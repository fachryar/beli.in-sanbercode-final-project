import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Spoonify from './index.js';

export default function App(){
    return(
        <Spoonify />
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
