import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button, TouchableOpacity, ImageBackground, Alert} from "react-native";
import * as firebase from "firebase";

export default function Login({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyBf2hiM7QjCHZ6Sj7PHOo8x3se9S7tUdDw",
        authDomain: "spoonify-7302d.firebaseapp.com",
        projectId: "spoonify-7302d",
        storageBucket: "spoonify-7302d.appspot.com",
        messagingSenderId: "1020200339475",
        appId: "1:1020200339475:web:5f43935795228e8dcfd7e6",
        measurementId: "G-BSN9B9NP0Z",
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    const login = () => {
        const data = {
        email,
        password,
        };

        firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
            console.log("Login Berhasil");
            navigation.navigate("MainApp");
        })
        .catch(() => {
            console.log("Login Gagal");
            Alert.alert(
                "Sign In Failed",
                "Incorrect email / password"
            )
        });
    };

    return (
        <View style={styles.container}>
            <View style ={{flex: 7}}>
                <ImageBackground style ={{width: 420, height: 500}} source={require('../assets/icon/login_bg.png')}>
                    <Image style={styles.logoPlacement} source={require('../assets/icon/logo.png')}/>
                </ImageBackground>
            </View>
            
            <View style={[styles.formContainer, {flex: 5}]}>
                <Text style={styles.formTitle}>Sign In</Text>
                <TextInput 
                    style={styles.inputForm}
                    placeholder="Email"
                    value={email}
                    onChangeText={(value) => setEmail(value)}/>
                <TextInput
                    style={styles.inputForm}
                    placeholder="Password"
                    value={password}
                    onChangeText={(value) => setPassword(value)}/>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={() => navigation.navigate("RegisterScreen")}>
                        <Text style={styles.smallRedButton}>Register</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={login}>
                        <Text style={styles.smallGreyButton}>Sign In</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F96C6C"
    },
    logoPlacement: {
        marginLeft: 32, 
        marginTop: 60,
        height: 50,
        width: 150
    },
    formContainer: {
        paddingHorizontal: 48,
        paddingVertical: 32,
        backgroundColor: 'white', 
        borderTopLeftRadius: 48,
        borderTopRightRadius: 48,
    },
    formTitle: {
        fontSize: 32,
        marginBottom: 16,
        fontWeight: 'bold',
        color: '#313131'
    },
    inputForm: {
        borderBottomWidth: 1,
        paddingVertical: 4,
        width: '100%',
        marginBottom: 24,
        borderColor: '#B3B3B3'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 32
    },
    smallGreyButton: {
        fontWeight: 'bold',
        color: '#313131'
    },
    smallRedButton: {
        fontWeight: 'bold',
        color: "#F96C6C"
    }
});
