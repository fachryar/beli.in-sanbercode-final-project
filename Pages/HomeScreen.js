import React, { useState, useEffect } from 'react'
import { FlatList, TouchableOpacity, StyleSheet, Text, View, Image, Button, Alert, Dimensions, TextInput } from 'react-native'
import axios from 'axios';
import * as firebase from 'firebase';

export default function HomeScreen({navigation}) {
    const [user, setUser] = useState({});
    const [totalPrice, setTotalPrice] = useState(0);
    const [items, setItems] = useState([]);

    useEffect(()=>{
      const userInfo = firebase.auth().currentUser
      setUser(userInfo);
    })

    useEffect(()=>{
        GetData()
    })

    const GetData=()=>{
        axios.get(`https://fakestoreapi.com/products?limit=3`)
        .then(res=>{
            const data1 = (res.data)
            // console.log('res: ', data1)
            setItems(data1)
        }).catch(err=>{
            console.log("error: ", err)
        })
    }

    return (
        <View style={{backgroundColor:'#F2F2F2', height:'100%'}}>
            <View style={styles.headerBackground}>
                <Image style={styles.headerLogo} source={require('../assets/icon/logo.png')}/>
                <View style={{flexDirection: 'row'}}>
                    <View style={styles.headerSearch}>
                        <Image style={styles.headerSearchIcon} source={require('../assets/icon/search.png')}/>   
                        <TextInput placeholder="Search. . ." style={{marginLeft: 10}}></TextInput>
                    </View>
                    <TouchableOpacity style={styles.greyIconHolder}>
                        <Image style={styles.greyIcon} source={require('../assets/icon/wishlist.png')}/>   
                    </TouchableOpacity>
                </View>    
            </View>

            <View style={{
                marginTop: -50,
                borderRadius: 16, 
                alignSelf: 'center'}}>
                    <Image style={{height: 100, width: 370,}} resizeMode='contain' source={require('../assets/banner.png')}/>
            </View>

            <View style={styles.categoryRowContainer}>
                <TouchableOpacity style={styles.categoryList}>
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_fashion.png')}/>
                        <Text style={styles.categoryText}>Fashion</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}>
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_electronics.png')}/>
                        <Text style={styles.categoryText}>Electronic</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}> 
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_health.png')}/>
                        <Text style={styles.categoryText}>Health</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}> 
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_food.png')}/>
                        <Text style={styles.categoryText}>Food & Drink</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.categoryRowContainer}>
                <TouchableOpacity style={styles.categoryList}>
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_hobby.png')}/>
                        <Text style={styles.categoryText}>Hobby</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}>
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_jewelry.png')}/>
                        <Text style={styles.categoryText}>Jewelry</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}> 
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_cosmetics.png')}/>
                        <Text style={styles.categoryText}>Cosmetics</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.categoryList}> 
                        <Image style={styles.categoryIcon} source={require('../assets/icon/cat_sport.png')}/>
                        <Text style={styles.categoryText}>Sports</Text>
                </TouchableOpacity>
            </View>

            <View style={{ backgroundColor: 'white', borderRadius: 16, marginTop: 16}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style={styles.titleCard}>New Arrivals</Text>
                    <Text style={styles.smallTextCard}>See All</Text>
                </View>
                <View style={{alignItems:'center', marginBottom: 6}}>
                    <FlatList
                        data={items}
                        numColumns={3}
                        keyExtractor={(item)=>item.id}
                        renderItem={({item}) => {
                            return(
                                <View>
                                    <TouchableOpacity 
                                    onPress={()=>{navigation.navigate("DetailScreen", {
                                        product: item
                                    })}} 
                                    style={styles.gridItem}>
                                        <Image
                                            style={styles.gridImage}
                                            source={{uri: item.image}}
                                            resizeMode='contain'
                                        />
                                        <Text numberOfLines={2} style={styles.gridTitle}>{item.title}</Text>
                                        <Text style={styles.gridPrice}>${item.price}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }}
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    headerBackground: {
        height: 150, 
        width: '100%', 
        backgroundColor: '#F96C6C',
        borderBottomLeftRadius: 32,
        borderBottomRightRadius: 32,
        flexDirection: 'row', 
        justifyContent: "space-between"
    },
    headerLogo: {
        marginLeft: 24, 
        marginTop: 40,
        height: 38,
        width: 100
    },
    headerTitle: {
        marginLeft: 24, 
        marginTop: 36,
        fontSize: 24,
        fontWeight: 'bold',
        color: '#EDEDED'
    },
    headerSearch: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: 180,
        height: 38, 
        marginTop: 36, 
        marginRight: 8,
        borderRadius: 8,
        backgroundColor: '#EDEDED'       
    },
    headerSearchIcon: {
        width: 20,
        height: 20,
        marginLeft: 10,
        tintColor: '#ACACAC'        
    },
    greyIconHolder:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 38,
        height: 38, 
        marginTop: 36, 
        marginRight: 24,
        borderRadius: 8,
        backgroundColor: '#EDEDED'
    },
    greyIcon:{
        width: 20,
        height: 20,
        tintColor: '#ACACAC'        
    },
    gridItem: {
        margin:4,
        width: 120,
        height: 180,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#F5F5F5',
        elevation: 0.5
    },
    gridImage: {
        height: 100,
        width: 100,
        margin: 10
    },
    gridTitle: {
        fontSize: 12,
        marginHorizontal:12,
        color: '#8A8A8A'
    },
    gridPrice: {
        fontSize: 13,
        fontWeight: 'bold',
        textAlign: 'left',
        marginHorizontal:12,
        color: '#494949'
    },
    titleCard: {
        marginLeft: 20,
        marginTop: 8,
        fontWeight: 'bold',
        fontSize: 18,
        color: '#F96C6C'        
    },
    smallTextCard:{
        color: '#F96C6C',
        fontSize: 12, 
        marginTop: 16,
        marginRight: 22,
        color: '#8A8A8A'
    },
    categoryIcon: {
        marginTop:16, 
        height:48, 
        width:48,
    },
    categoryText: {
        fontSize: 12,
        color: '#8A8A8A'
    },
    categoryRowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginHorizontal:16
    },
    categoryList: {
        flex: 1,
        alignItems: 'center'
    } 
})
