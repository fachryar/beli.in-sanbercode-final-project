import React from 'react'
import { StyleSheet, Text, View, Image, Button, TouchableOpacity } from 'react-native'

export default function DetailScreen({route, navigation}) {
    const { product } = route.params;
    return (
        <View style={{backgroundColor:'#F2F2F2'}}>
            <View style={{backgroundColor:'#FFF', paddingVertical:40}}> 
                
                <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                    <TouchableOpacity onPress={()=> {navigation.navigate('MainApp')}}>
                        <Image
                            style={styles.topLeftIcon}
                            source={require('../assets/icon/left-arrow.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image
                            style={styles.topRightIcon}
                            source={require('../assets/icon/wishlist.png')}
                        />
                    </TouchableOpacity> 
                </View>
                <Image
                    style={styles.imagePreview}
                    source={{uri: product.image}}
                    resizeMode='contain'
                />
                
            </View>
            <View style={styles.detailCard}>
                <Text style={styles.textCategory}>{product.category}</Text>
                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.textTitle}>{product.title}</Text>
                    <Text style={styles.textPrice}>${product.price}</Text>
                </View>
                <Text style={styles.textDesc}>{product.description}</Text>
                <View style={{flex: 1, justifyContent: 'flex-end'}}>
                    <TouchableOpacity style={styles.buttonContainer}>
                        <Image style={styles.buttonIcon} source={require('../assets/icon/cart.png')}/>
                        <Text style={styles.buttonText}>Add to Cart</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
    },
    topLeftIcon:{
        height: 16,
        width: 16,
        marginLeft:16,
        tintColor: '#4F4F4F'
    },
    topRightIcon:{
        height: 24,
        width: 24,
        marginRight:20,
        tintColor: '#F96C6C'
    },
    imagePreview:{
        height:300,
        width: '100%'
    },
    detailCard:{
        height: 310,
        backgroundColor:'#FFFFFF', 
        borderTopLeftRadius: 32,
        borderTopRightRadius: 32,
        marginTop: -30,
        elevation: 30,
    },
    textTitle: {
        flex: 5,
        marginHorizontal:20,
        fontWeight: 'bold',
        fontSize: 18,
        color: '#4D4F51'
    },
    textCategory: {
        marginTop:40,
        marginHorizontal:20,
        color: '#8A8A8A'
    },
    textPrice: {
        flex: 2,
        marginHorizontal:20,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'right',
        color: '#F96C6C'
    },
    textDesc: {
        marginHorizontal:20,
        marginTop:12,
        color: '#8A8A8A',
        textAlign: 'justify'
    },
    buttonContainer: {
        flexDirection: 'row',
        height: 50,
        width: '80%',
        backgroundColor: '#F96C6C',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 16,
        elevation: 2,
        marginBottom: 30
    },
    buttonText: {
        alignSelf: "center",
        fontWeight: "bold",
        color: '#FFF',
        fontSize: 16
    },
    buttonIcon:{
        alignSelf: "center",
        height: 20,
        width: 20,
        marginRight:8,
        tintColor: '#FFF'
    }
})