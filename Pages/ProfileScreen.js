import React from 'react'
import { Button, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import * as firebase from 'firebase'

export default function ProfileScreen({navigation}) {

    const logout = ()=>{
        firebase.auth().signOut().then(()=>{
            console.log("User Berhasil Logout")
            navigation.navigate("LoginScreen")
        })
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={{flex: 5, flexDirection: 'row'}}>
                    <Image style={styles.profilePicture} source={require('../assets/icon/avatar.png')}/>
                    <View>
                        <Text style={styles.profileTextName}>Emilia Rose Clarke</Text>
                        <Text style={styles.profileTextMember}>Premium Member</Text>
                    </View>
                </View>
                <TouchableOpacity style={{flex: 1,width: 150, justifyContent: 'center'}}>
                    <Image style={[styles.greyIcon, {alignSelf: 'flex-end'}]} source={require('../assets/icon/edit.png')}/>
                </TouchableOpacity>
            </View>

            <View style={styles.containerNumber}>
                <View>
                    <Text style={styles.textNumberBig}>7</Text>
                    <Text style={styles.textBelowIcon}>Cart</Text>
                </View>
                <View>
                    <Text style={styles.textNumberBig}>8</Text>
                    <Text style={styles.textBelowIcon}>Wishlist</Text>
                </View>
                <View>
                    <Text style={styles.textNumberBig}>109</Text>
                    <Text style={styles.textBelowIcon}>Order</Text>
                </View>
                <View>
                    <Text style={styles.textNumberBig}>24</Text>
                    <Text style={styles.textBelowIcon}>Voucher</Text>
                </View>
            </View>

            <View style={styles.menuCardContainer}>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_lastseen.png')}/>
                            <Text style={styles.textMenu}>Last Seen</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <View style={styles.menuLine}/>
                </View>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_review.png')}/>
                            <Text style={styles.textMenu}>Review</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <View style={styles.menuLine}/>
                </View>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_complaint.png')}/>
                            <Text style={styles.textMenu}>Complaint</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <View style={styles.menuLine}/>
                </View>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_favorite.png')}/>
                            <Text style={styles.textMenu}>Favorite Shop</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <View style={styles.menuLine}/>
                </View>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_address.png')}/>
                            <Text style={styles.textMenu}>My Address</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={[styles.menuCardContainer, {marginTop:12}]}>
                <View>
                    <TouchableOpacity style={styles.menuListContainer}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_setting.png')}/>
                            <Text style={styles.textMenu}>Setting</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.arrowIcon} source={require('../assets/icon/right-arrow.png')}/>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    <View style={styles.menuLine}/>
                </View>
                <View>
                    <TouchableOpacity style={styles.menuListContainer} onPress={logout}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.colorIcon} source={require('../assets/icon/square_logout.png')}/>
                            <Text style={styles.textMenu}>Log Out</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: 48,
        paddingHorizontal: 20
    },
    profilePicture:{
        height: 50,
        width: 50,
        borderRadius: 25,
        marginRight: 12
    },
    profileTextName: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#4D4F51'
    },
    profileTextMember: {
        color: '#8A8A8A'
    },
    containerNumber: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 12,
        marginVertical: 20,
    },
    textNumberBig: {
        textAlign:'center',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#434343'
    },
    textBelowIcon: {
        textAlign:'center',
        color: '#8A8A8A'
    },
    textMenu: {
        fontSize: 16,
        marginLeft: 12,
        color: '#5B5E60'
    },
    greyIcon:{
        height: 16,
        width: 16,
        tintColor: '#8A8A8A',
        marginRight: 8       
    },
    colorIcon:{
        height: 24,
        width: 24    
    },
    arrowIcon:{
        height: 12,
        width: 12,
        tintColor: '#D7D7D7',       
    },
    menuCardContainer: {
        borderRadius: 8,
        elevation: 2,
        backgroundColor: '#FFF'
    },
    menuListContainer: {
        flexDirection: 'row',
        padding: 16,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    menuLine: {
        borderWidth: 1, 
        borderColor: '#F4F4F4'
    }
})

